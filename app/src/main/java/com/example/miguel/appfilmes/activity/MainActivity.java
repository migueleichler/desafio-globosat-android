package com.example.miguel.appfilmes.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.miguel.appfilmes.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = (Button) findViewById(R.id.btnEntrar);
        final TextView linkInscricao = (TextView) findViewById(R.id.txtInscricao);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent listaFilmesIntent = new Intent(MainActivity.this, ListaFilmesActivity.class);
                MainActivity.this.startActivity(listaFilmesIntent);
            }
        });

        linkInscricao.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                 Intent inscricaoIntent = new Intent(MainActivity.this, InscricaoActivity.class);
                 MainActivity.this.startActivity(inscricaoIntent);
             }
        });
    }
}
