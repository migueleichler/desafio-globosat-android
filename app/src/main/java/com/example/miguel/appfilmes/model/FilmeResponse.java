package com.example.miguel.appfilmes.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class FilmeResponse {

        @SerializedName("page")
        private int page;
        @SerializedName("results")
        private List<Filme> results;
        @SerializedName("total_results")
        private int totalResults;
        @SerializedName("total_pages")
        private int totalPages;

        public int getPage() { return page; }

        public void setPage(int page) {
            this.page = page;
        }

        public List<Filme> getResults() {
            return results;
        }

        public void setResults(List<Filme> results) { this.results = results; }

        public int getTotalResults() {
            return totalResults;
        }

        public void setTotalResults(int totalResults) {
            this.totalResults = totalResults;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }
}
