package com.example.miguel.appfilmes.service;

import com.example.miguel.appfilmes.model.FilmeResponse;

import retrofit2.http.GET;
import retrofit2.Call;


public interface FilmeApiService {
    @GET("bins/x157z")
    Call<FilmeResponse> getListaFilmes();

}

