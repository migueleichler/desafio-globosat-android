package com.example.miguel.appfilmes.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.miguel.appfilmes.model.Filme;
import com.example.miguel.appfilmes.R;

import java.util.List;

import com.squareup.picasso.Picasso;

public class FilmeAdapter extends RecyclerView.Adapter<FilmeAdapter.FilmeViewHolder> {

    private List<Filme> filmes;
    private int rowLayout;
    private Context context;

    public FilmeAdapter(List<Filme> filmes, int rowLayout, Context context) {
        this.filmes = filmes;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    public static class FilmeViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout_filmes;
        TextView titulo;
        TextView subtitulo;
        TextView sinopse;
        ImageView thumb;

        public FilmeViewHolder(View view) {
            super(view);
            layout_filmes = (LinearLayout) view.findViewById(R.id.layout_filmes);
            thumb = (ImageView) view.findViewById(R.id.thumb);
            titulo = (TextView) view.findViewById(R.id.titulo);
            subtitulo = (TextView) view.findViewById(R.id.subtitulo);
            sinopse = (TextView) view.findViewById(R.id.sinopse);
        }
    }


    @Override
    public FilmeAdapter.FilmeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new FilmeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FilmeViewHolder holder, final int position) {
        String url_imagem = filmes.get(position).getThumb();
        Picasso.with(context)
                .load(url_imagem)
                .placeholder(android.R.drawable.sym_def_app_icon)
                .error(android.R.drawable.sym_def_app_icon)
                .into(holder.thumb);
        holder.titulo.setText(filmes.get(position).getTitulo());
        holder.subtitulo.setText(filmes.get(position).getSubtitulo());
        holder.sinopse.setText(filmes.get(position).getSinopse());
    }

    @Override
    public int getItemCount() {
        return filmes.size();
    }
}
