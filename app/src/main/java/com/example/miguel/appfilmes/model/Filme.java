package com.example.miguel.appfilmes.model;

import com.google.gson.annotations.SerializedName;


public class Filme {
    @SerializedName("titulo")
    private String titulo;
    @SerializedName("subtitulo")
    private String subtitulo;
    @SerializedName("duracao")
    private String duracao;
    @SerializedName("sinopse")
    private String sinopse;
    @SerializedName("thumb")
    private String thumb;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubtitulo() { return subtitulo; }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public String getSinopse() { return sinopse; }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public String getThumb() { return thumb; }

}
