package com.example.miguel.appfilmes.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.miguel.appfilmes.R;
import com.example.miguel.appfilmes.adapter.FilmeAdapter;
import com.example.miguel.appfilmes.model.Filme;
import com.example.miguel.appfilmes.model.FilmeResponse;
import com.example.miguel.appfilmes.service.FilmeApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListaFilmesActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String BASE_URL = "http://api.myjson.com/";
    private static Retrofit retrofit = null;
    private RecyclerView recyclerView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_filmes);

        recyclerView = (RecyclerView) findViewById(R.id.layout_filmes);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getListaFilmes();
    }

    public void getListaFilmes() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        FilmeApiService filmeApiService = retrofit.create(FilmeApiService.class);

        Call<FilmeResponse> call = filmeApiService.getListaFilmes();
        call.enqueue(new Callback<FilmeResponse>() {
            @Override
            public void onResponse(Call<FilmeResponse> call, Response<FilmeResponse> response) {
                List<Filme> filmes = response.body().getResults();
                recyclerView.setAdapter(new FilmeAdapter(filmes, R.layout.item_lista_filmes, getApplicationContext()));
                Log.d(TAG, "Total de filmes: " + filmes.size());
            }

            @Override
            public void onFailure(Call<FilmeResponse> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });
    }
}
