package com.example.miguel.appfilmes.db_connection;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MainGenerator {
    public static void main(String[] args)  throws Exception {

        Schema schema = new Schema(1, "com.codekrypt.greendao.db");

        Entity usuario = schema.addEntity("USUARIO");
        usuario.addIdProperty();
        usuario.addStringProperty("email").notNull();
        usuario.addStringProperty("usuario").notNull();

        Entity favorito = schema.addEntity("FAVORITO");
        favorito.addIdProperty();
        favorito.addStringProperty("titulo").notNull();
        favorito.addStringProperty("subtitulo").notNull();
        favorito.addStringProperty("duracao").notNull();
        favorito.addStringProperty("sinopse").notNull();
        favorito.addStringProperty("thumb").notNull();

        new DaoGenerator().generateAll(schema, "./app/src/main/java");

    }
}
